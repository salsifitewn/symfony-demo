<?php

namespace App\Tests\Controller;

use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RegistrationControllerTest extends WebTestCase
{
    public function testRoute()
    {
        $client = static::createClient();
        $client->request('GET', '/register');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Inscription');
    }

    // public function testRegistrationSubmission()
    // {
    //     $client = static::createClient();
    //     $client->request('GET', '/en/conference/amsterdam-2019');
    //     $client->submitForm('Submit', [
    //         'registrationForm[email]' => $email = 'test@test.com',
    //         'registrationForm[name]' => 'Fredéric',
    //         'registrationForm[lastName]' => 'Germain',
    //         'registrationForm[plainPassword]' => 'password',
    //     ]);
    //     $this->assertResponseRedirects();
    //     $comment = self::getContainer()->get(UserRepository::class)->findOneByEmail($email);

    //     // simulate comment validation
    //     $comment = self::getContainer()->get(CommentRepository::class)->findOneByEmail($email);
    //     $comment->setState('published');
    //     self::getContainer()->get(EntityManagerInterface::class)->flush();

    //     $client->followRedirect();
    //     $this->assertSelectorExists('div:contains("There are 2 comments")');
    // }
    //ici
    public function testRegistration2Submission()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $form = $crawler->selectButton('Inscrire')->form();
        $form['registration_form[email]'] = 'test2@example.com';
        $form['registration_form[name]'] = 'John';
        $form['registration_form[lastName]'] = 'Doe';
        $form['registration_form[plainPassword]'] = 'password';

        $client->submit($form);

        $this->assertResponseRedirects('/login'); // Assuming successful registration redirects to homepage

        // Check if the user is inserted in the database
        $userRepository = $client->getContainer()->get('doctrine')->getRepository(User::class);
        $user = $userRepository->findOneBy(['email' => 'test2@example.com']);

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals('John', $user->getName());
        $this->assertEquals('Doe', $user->getLastName());

        self::$kernel->getContainer()->get('doctrine')->getConnection()->rollBack();
    }
}
