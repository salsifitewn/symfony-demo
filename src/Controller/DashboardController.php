<?php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class DashboardController extends AbstractController
{


    #[Route('/home', methods: ['GET', 'HEAD','POST'],name:'home')]
    public function home(Request $request): Response
    {

      return $this->render('dashboard.html.twig');
    }
}